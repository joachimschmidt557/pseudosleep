#requires -runasadministrator

Param(
    [ValidateRange(0,3)][int]$SleepStage
)

# STAGES
# 0 - Do nothing
# 1 - Suspend processes
# 2 - Suspend all processes
# 3 - Suspend all processes and disable devices

#nircmd.exe lockws
#nircmd.exe monitor off

#.\white.exe

if ((Get-Module Pause-Process) -eq $null)
{
    Import-Module .\pause-process.ps1
}

#if ((Get-Process DontSleep_x64 -ErrorAction SilentlyContinue) -eq $null)
#{
#    Start-Process DontSleep_x64.exe
#}
#
#Invoke-WebRequest http://localhost:8080/?enable=1
#
#Invoke-WebRequest http://localhost:8080/?block_screensaver=0

$musicPowerPlan = Get-CimInstance -Name root\cimv2\power -Class win32_PowerPlan -Filter "ElementName = 'Music'"
$balancedPowerPlan = Get-CimInstance -Name root\cimv2\power -Class win32_PowerPlan -Filter "ElementName = 'Balanced'"
$x = Invoke-CimMethod -InputObject $musicPowerPlan -MethodName Activate

function SuspendProcess ($process) {
    #Start-Process pssuspend64.exe -ArgumentList $process.ID.ToString() -NoNewWindow -Wait -PassThru
    Pause-Process -ID $process.ID
}

function ResumeProcess ($process) {
    #Start-Process pssuspend64.exe -ArgumentList "-r", $process.ID.ToString() -NoNewWindow -Wait -PassThru
    Unpause-Process -ID $process.ID
}

$currentUser = whoami.exe
$includedUserNames = @($currentUser)
$excludedProcesses = @("DeezPlayer", "Music.UI")
$suspendedProcesses = @()

Get-Process -IncludeUserName | Where-Object {$_.Username -ne $null} | ForEach-Object {
#    if ($includedUserNames -contains $_.Username.ToLower() -And !($_.Path.ToLower().StartsWith("c:\windows\system32\")) ) 
    if (!($_.Path.ToLower().StartsWith("c:\windows\system32\")) ) 
    {
        if (!($excludedProcesses -contains $_.Name))
        {
            #SuspendProcess $_
            $suspendedProcesses += $_
        }
    }
}
Write-Host Sleeping

#echo $suspendedProcesses

Write-Host "Press any key to exit pseudo-sleep ..."
$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")

#nircmd.exe monitor off

$suspendedProcesses | ForEach-Object {
    #ResumeProcess $_
}

#nircmd.exe monitor on

#pskill.exe white

#Invoke-WebRequest http://localhost:8080/?enable=0

$x = Invoke-CimMethod -InputObject $balancedPowerPlan -MethodName Activate
